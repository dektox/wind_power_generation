### Parameters to load
To predict actual wind speed for 12/27/2017 22:45:00
Parameters:
   * time: "12/27/2017 22:45:00"
   * location_1_lagged: wind speed at location 1 with 4-hours time lag, on 12/27/2017 18:45:00. Value: 7.8
   * location_2: forecasted wind speed in location 2 at the prediction time, on 12/27/2017 22:45:00. Value: 5.261
   * location_3: forecasted wind speed in location 3 at the prediction time, on 12/27/2017 22:45:00. Value: 2.594
   * location_4: forecasted wind speed in location 4 at the prediction time, on 12/27/2017 22:45:00. Value: 6.710
   * location_5: forecasted wind speed in location 5 at the prediction time, on 12/27/2017 22:45:00. Value: 2.437
   * location_6: forecasted wind speed in location 6 at the prediction time, on 12/27/2017 22:45:00. Value: 5.997
   * location_7: forecasted wind speed in location 7 at the prediction time, on 12/27/2017 22:45:00. Value: 2.391
   * location_8: forecasted wind speed in location 8 at the prediction time, on 12/27/2017 22:45:00. Value: 4.439
   * location_9: forecasted wind speed in location 9 at the prediction time, on 12/27/2017 22:45:00. Value: 1.796
   * location_10: forecasted wind speed in location 10 at the prediction time, on 12/27/2017 22:45:00. Value: 4.633
   * location_11: forecasted wind speed in location 11 at the prediction time, on 12/27/2017 22:45:00. Value: 1.744
   * location_12: forecasted wind speed in location 12 at the prediction time, on 12/27/2017 22:45:00. Value: 3.636
   * location_13: forecasted wind speed in location 13 at the prediction time, on 12/27/2017 22:45:00. Value: 1.066
   * wind_direction: forecasted wind direction at the prediction time, on 12/27/2017 22:45:00. Value: 116.358
   * humidity: forecasted humidity at the prediction time, on 12/27/2017 22:45:00. Value: 53.4991
   * temperature: forecasted temperature at the prediction time, on 12/27/2017 22:45:00. Value: 2.17337
   * pressure: forecasted pressure at the prediction time, on 12/27/2017 22:45:00. Value: 909.29
   * actual_power_lagged: actual power with 4-hours time lag, on 12/27/2017 18:45:00. Value: 16.634
   * max_capacity_lagged: actual power with 4-hours time lag, on 12/27/2017 18:45:00. Value: 40.75

To predict actual wind speed for 12/27/2017 23:00:00
Parameters:
   * time: "12/27/2017 23:00:00"
   * location_1_lagged: wind speed at location 1 with 4-hours time lag, on 12/27/2017 19:00:00. Value: 8.4
   * location_2: forecasted wind speed in location 2 at the prediction time, on 12/27/2017 23:00:00. Value: 5.182
   * location_3: forecasted wind speed in location 3 at the prediction time, on 12/27/2017 23:00:00. Value: 2.57
   * location_4: forecasted wind speed in location 4 at the prediction time, on 12/27/2017 23:00:00. Value: 6.644
   * location_5: forecasted wind speed in location 5 at the prediction time, on 12/27/2017 23:00:00. Value: 2.386
   * location_6: forecasted wind speed in location 6 at the prediction time, on 12/27/2017 23:00:00. Value: 5.913
   * location_7: forecasted wind speed in location 7 at the prediction time, on 12/27/2017 23:00:00. Value: 2.316
   * location_8: forecasted wind speed in location 8 at the prediction time, on 12/27/2017 23:00:00. Value: 4.323
   * location_9: forecasted wind speed in location 9 at the prediction time, on 12/27/2017 23:00:00. Value: 1.746
   * location_10: forecasted wind speed in location 10 at the prediction time, on 12/27/2017 23:00:00. Value: 4.52
   * location_11: forecasted wind speed in location 11 at the prediction time, on 12/27/2017 23:00:00. Value: 1.678
   * location_12: forecasted wind speed in location 12 at the prediction time, on 12/27/2017 23:00:00. Value: 3.574
   * location_13: forecasted wind speed in location 13 at the prediction time, on 12/27/2017 23:00:00. Value: 1.042
   * wind_direction: forecasted wind direction at the prediction time, on 12/27/2017 23:00:00. Value: 116.624
   * humidity: forecasted humidity at the prediction time, on 12/27/2017 23:00:00. Value: 53.6761
   * temperature: forecasted temperature at the prediction time, on 12/27/2017 23:00:00. Value: 2.08723
   * pressure: forecasted pressure at the prediction time, on 12/27/2017 23:00:00. Value: 909.301
   * actual_power_lagged: actual power with 4-hours time lag, on 12/27/2017 19:00:00. Value: 17.48
   * max_capacity_lagged: actual power with 4-hours time lag, on 12/27/2017 19:00:00. Value: 40.75
   
   
### GET request from browser

```
http://34.221.165.225:8000/predict?time="12/27/2017 22:45:00","12/27/2017 23:00:00"&location_1_lagged=7.8,8.4&location_2=5.261,5.182&location_3=2.594,2.57&location_4=6.71,6.644&location_5=2.437,2.386&location_6=5.997,5.913&location_7=2.391,2.316&location_8=4.439,4.323&location_9=1.796,1.746&location_10=4.633,4.52&location_11=1.744,1.678&location_12=3.636,3.574&location_13=1.066,1.042&wind_direction=116.358,116.624&humidity=53.4991,53.6761&temperature=2.17337,2.08723&pressure=909.29,909.301&actual_power_lagged=16.634,17.48&max_capacity_lagged=40.75,40.75
```

### PYTHON code:

```
import requests
import json

page = 'http://34.221.165.225:8000/predict'
payload = {"time": ["12/27/2017 22:45:00", "12/27/2017 23:00:00"],
           "location_1_lagged": [7.8, 8.4],
           "location_2": [5.261, 5.182],
           "location_3": [2.594, 2.57],
           "location_4": [6.710, 6.644],
           "location_5": [2.437, 2.386],
           "location_6": [5.997, 5.913],
           "location_7": [2.391, 2.316],
           "location_8": [4.439, 4.323],
           "location_9": [1.796, 1.746],
           "location_10": [4.633, 4.52],
           "location_11": [1.744, 1.678],
           "location_12": [3.636, 3.574],
           "location_13": [1.066, 1.042],
           "wind_direction": [116.358, 116.624],
           "humidity": [53.4991, 53.6761],
           "temperature": [2.17337, 2.08723],
           "pressure": [909.29, 909.301],
           "actual_power_lagged": [16.634, 17.48],
           "max_capacity_lagged": [40.75, 40.75],
           }

html = requests.get(page, data=payload).text
response = json.loads(html)
print(response)
```
