# Windmills Power Prediction API 

## API Methods
API has one GET method /predict.

### GET /predict
Parameters:
   * time: optional parameter. Time for which power is predicted
   * location_1_lagged: wind speed at location 1 with 4-hours time lag. Example: if you need to forecast power for 23:00:00, you have to input wind speed in location 1 at 19:00:00
   * location_2: forecasted wind speed in location 2 at the prediction time. Example: if you need to forecast power for 23:00:00, you have to input forecasted wind speed in location 2 at 23:00:00
   * location_3: forecasted wind speed in location 3 at the prediction time;
   * location_4: forecasted wind speed in location 4 at the prediction time;
   * location_5: forecasted wind speed in location 5 at the prediction time;
   * location_6: forecasted wind speed in location 6 at the prediction time;
   * location_7: forecasted wind speed in location 7 at the prediction time;
   * location_8: forecasted wind speed in location 8 at the prediction time;
   * location_9: forecasted wind speed in location 9 at the prediction time;
   * location_10: forecasted wind speed in location 10 at the prediction time;
   * location_11: forecasted wind speed in location 11 at the prediction time;
   * location_12: forecasted wind speed in location 12 at the prediction time;
   * location_13: forecasted wind speed in location 13 at the prediction time;
   * wind_direction: forecasted wind direction at the prediction time. Example: if you need to forecast power for 23:00:00, you have to input forecasted wind direction at 23:00:00
   * humidity: forecasted humidity at the prediction time. Example: if you need to forecast power for 23:00:00, you have to input forecasted humidity at 23:00:00
   * temperature: forecasted temperature at the prediction time. Example: if you need to forecast power for 23:00:00, you have to input forecasted temperature at 23:00:00
   * pressure: forecasted pressure at the prediction time. Example: if you need to forecast power for 23:00:00, you have to input forecasted pressure at 23:00:00
   * actual_power_lagged: actual power with 4-hours time lag. Example: if you need to forecast power for 23:00:00, you have to input actual power at 19:00:00
   * max_capacity_lagged: actual power with 4-hours time lag. Example: if you need to forecast power for 23:00:00, you have to input max capacity at 19:00:00

All the parameters except time are required. NAs in the dataset aren't allowed.
Prediction allowed for 1 or more time points.

Example of the request for API for 2 time point (if API is run on localhost, 8000 port):

```
http://34.221.165.225:8000/predict?time="2019/01/01 18:00:00","2019/01/01 18:15:00"&location_1_lagged=1,2&location_2=2,3&location_3=3,4&location_4=4,5&location_5=5,6&location_6=6,7&location_7=7,8&location_8=8,9&location_9=9,10&location_10=10,11&location_11=11,12&location_12=12,13&location_13=13,14&wind_direction=14,15&humidity=15,16&temperature=16,17&pressure=17,18&actual_power_lagged=18,19&max_capacity_lagged=19,20
```

Example of the request for API for 2 time point, Python code

```
import requests
import json

page = 'http://34.221.165.225:8000/predict'
payload = {"time": ["2019/01/01 18:00:00", "2019/01/01 18:15:00"],
           "location_1_lagged": [1, 2],
           "location_2": [2, 3],
           "location_3": [3, 4],
           "location_4": [4, 5],
           "location_5": [5, 6],
           "location_6": [6, 7],
           "location_7": [7, 8],
           "location_8": [8, 9],
           "location_9": [9, 10],
           "location_10": [10, 11],
           "location_11": [11, 12],
           "location_12": [12, 13],
           "location_13": [13, 14],
           "wind_direction": [14, 15],
           "humidity": [15, 16],
           "temperature": [16, 17],
           "pressure": [17, 18],
           "actual_power_lagged": [18, 19],
           "max_capacity_lagged": [19, 20]
           }

html = requests.get(page, data=payload).text
response = json.loads(html)
print(response)
```

## Setup API On Server

The example is for setting API in Docker. You have to previously install Docker on the server.

Step 1. Create directory for API.

```
mkdir windmills_api_files
```

Step 2. Change directory

```
cd windmills_api_files
```

Step 3. Copy files to windmills_api_files from a local computer. 

Step 4. Create Dockerfile with the next input and save it.

```
nano Dockerfile
``` 

Step 5. Make the next input in Dockerfile and save it.

```
	FROM trestletech/plumber

	RUN install2.r randomForest
	RUN install2.r jsonlite

	COPY rf_api.R .
	COPY windmills_rf_lagged.Rda .

	CMD ["rf_api.R"]
```

Step 6. Build API in Docker

```
docker build -t windmills_api:latest .
```

Step 7. Run API on 80 port

```
docker run -p 80:8000 windmills_api:latest
```
