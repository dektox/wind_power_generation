import requests
import json

# page = 'http://35.210.173.156/predict'
page = 'http://localhost:8000/predict'
payload = {"time": ["2019/01/01 18:00:00", "2019/01/01 18:15:00"],
           "location_1_lagged": [1, 2],
           "location_2": [2, 3],
           "location_3": [3, 4],
           "location_4": [4, 5],
           "location_5": [5, 6],
           "location_6": [6, 7],
           "location_7": [7, 8],
           "location_8": [8, 9],
           "location_9": [9, 10],
           "location_10": [10, 11],
           "location_11": [11, 12],
           "location_12": [12, 13],
           "location_13": [13, 14],
           "wind_direction": [14, 15],
           "humidity": [15, 16],
           "temperature": [16, 17],
           "pressure": [17, 18],
           "actual_power_lagged": [18, 19],
           "max_capacity_lagged": [19, 20],
           }

html = requests.get(page, data=payload).text
response = json.loads(html)
print(response)
